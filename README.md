# Projet de configurateur de véhicule

## Contexte du projet

Votre employeur a été engagé par BestDeals Ltd, société basée aux États-Unis.

Ils souhaitent lancer une plateforme de revente en ligne de véhicules d'occasion qu'ils achètent aux enchères.

Dans un premier temps, ils vont vendre des vélos, des motos, des scooters, et des voitures. Ils se réservent la possibilité à l'avenir de vendre des camions, des bateaux, des drones, des avions, la seule limite étant leur imagination.

Votre manager a décidé de vous confier le développement du prototype du formulaire de saisie de véhicule, qui sera accessible dans le panneau d'administration du site.

Vous avez une semaine pour présenter un prototype fonctionnel au client. Vous devez donc créer trois pages webs :

une page avec la liste des véhicules enregistrés dansla base de données
une page avec la fiche d'un véhicule
le plus important, un formulaire permettant la saisie d'un nouveau véhicule
​
Le formulaire doit permettre de choisir le type de véhicule, puis de saisir les informations correspondantes (marque, couleur, date d'achat, prix d'achat, prix de revente HT, et toutes les informations classiques pour chaque type de véhicule).

Les trois pages doivent être fonctionnelles (pas forcément jolies). Le formulaire doit permettre d'ajouter un vélo, une moto, ou une voiture au choix. Les éléments communs aux trois véhicules, ainsi que 2 ou 3 éléments spécifiques à chacun d'entre eux doivent être présents.

Par exemple : on doit pouvoir saisir la marque, la date d'acquisition, la couleur, et le prix de vente de chacun des véhicules. Pour les vélos, on va ajouter le type de vélo (route, VTT, etc), le nombre de plateaux, s'il y a un marquage anti-vol ou pas. Pour les véhicules à moteur, le kilométrage et la date de première mise en circulation. Pour les voitures, on va indiquer le type, le nombre de portes, si la boîte de vitesse est mécanique ou automatique, et la puissance en chevaux. Pour les motos, la cylindrée, la puissance, et le type.

Attention, le formulaire ne doit suggérer que les valeurs cohérentes avec le véhicule choisi.

## Base de donnée

Afin de réaliser ce projet j'ai décidé de créer une base de données avec 4 entités:
 - l'entité Vehicle
 - l'entité Car qui héritera de Vehicle
 - l'entié Motorcycle qui héritera de Vehicle
 - l'entité Bike qui héritera de Vehicle
L'héritage n'existant pas en base de données chacune des 3 entités enfant sera en relation OneToOne avec Vehicle.
Les diagrammes ont été fait via StarUML.

## Design Pattern

Pour ce projet, l'une des consignes était d'intégrer des Designe Pattern.
J'ai donc décider d'utiliser le pattern Factory et le pattern Builder.

 - Le pattern Factory va me permettre de n'avoir qu'un seul controleur qui fera appel à une classe VehicleFactory afin d'utiliser le bon repository en fonction du type de véhicule à créer/rechercher à l'aide d'un Switch Case. Si on souhaite ajouter d'autres vehicules à l'avenir, cela permettra de n'avoir que la factory à modifier.

 - Le pattern Builder me permet de traiter le retour en JSON de l'application Front,  afin de pouvoir l'enregistrer en base de données.

 ## Front

 J'ai créer un repository [ici](https://gitlab.com/ThomasSeb/front-vehicle-configurator), qui sera un Front basique en ReactJs qui affichera:
  - une page avec la liste des véhicules et la possibilité d'en ajouter un.
  - une page avec pour voir un véhicule en particulier en cliquant dessus depuis la liste, avec un bouton pour le supprimer.
  - une page avec le formulaire où l'on pourra choisir quel type de véhicule créer.
