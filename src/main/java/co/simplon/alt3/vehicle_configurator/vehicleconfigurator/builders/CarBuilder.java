package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Car;

public class CarBuilder {

    private Car car;

    public Car getCar(String json ){

        try {
            car = new ObjectMapper().readValue(json, Car.class);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return car;
    }
}
