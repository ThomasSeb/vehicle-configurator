package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Vehicle;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.factories.VehicleFactory;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories.VehicleRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000") /* Pour donner l'accès à mon front en react et éviter le problème de CORS*/
public class VehicleController {

    @Autowired
    private VehicleRepository repo;

    @Autowired
    private VehicleFactory factory;
    
    @GetMapping("/vehicle")
    public List<Vehicle> allVehicleByType() {

        return repo.findAll(Sort.by(Sort.Order.asc("vehicleType")));
    }

    @GetMapping("/vehicle/{id}")
    public Vehicle showOneVehicle(@PathVariable int id) {
        return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));   
    }
    
    @GetMapping("/vehicle/{type}/{id}")
    public Vehicle findOneVehicleByType(@PathVariable int id, @PathVariable String type){
        Vehicle vehicle = factory.getVehicle(id, type);
        if (vehicle == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return vehicle;
        
    }

    @PostMapping("/vehicle/add")
    public Vehicle addVehicle(@RequestBody String veh){
        return factory.addVehicle(veh);
        
    }

    @DeleteMapping("/vehicle/{id}")
    public void deleteVehicle(@PathVariable int id){
        repo.deleteById(id);
    }

}
