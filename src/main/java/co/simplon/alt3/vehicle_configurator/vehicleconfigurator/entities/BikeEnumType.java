package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

public enum BikeEnumType {
    MOUNTAIN,
    ROAD,
    RACING
}
