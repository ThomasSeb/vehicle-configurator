package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

public enum MotorcycleEnumType {
    ROAD,
    ROADSTER,
    SPORTS,
    SUPERMOTO,
    CUSTOM
}
