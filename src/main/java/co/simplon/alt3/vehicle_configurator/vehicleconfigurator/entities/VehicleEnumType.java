package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

public enum VehicleEnumType {
    CAR,
    MOTORCYCLE,
    BIKE
}
