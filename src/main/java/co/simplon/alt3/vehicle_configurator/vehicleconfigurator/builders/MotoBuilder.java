package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Motorcycle;

public class MotoBuilder {
    private Motorcycle moto;
    public Motorcycle getMoto(String json ){
        try {
            moto = new ObjectMapper().readValue(json, Motorcycle.class);
            
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return moto;
    }
}
