package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.factories;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders.BikeBuilder;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders.CarBuilder;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders.MotoBuilder;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Bike;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Car;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Motorcycle;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Vehicle;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories.BikeRepository;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories.CarRepository;
import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories.MotorcycleRepository;

@Component
public class VehicleFactory {

    private String type;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private MotorcycleRepository motoRepository;

    @Autowired
    private BikeRepository bikeRepository;
    
    public Vehicle getVehicle(int id, String type){
        switch (type) {
            case "CAR":
                Car car = carRepository.findById(id).get();
                return car;
            case "MOTORCYCLE":
                Motorcycle moto = motoRepository.findById(id).get();
                return moto;
            case "BIKE":
                Bike bike = bikeRepository.findById(id).get();
                return bike;
            default:
                return null;
        }
    }

    public Vehicle addVehicle(String vehicle){
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(vehicle);
            JsonNode typeNode = rootNode.path("vehicleType");
            type = typeNode.asText();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
 
        switch (type) {
            case "CAR":
                CarBuilder carBuilder = new CarBuilder();
                Car car = carRepository.save(carBuilder.getCar(vehicle));
                return car;
            case "MOTORCYCLE":
                MotoBuilder motoBuilder = new MotoBuilder();
                Motorcycle moto = motoRepository.save(motoBuilder.getMoto(vehicle));
                return moto;
            case "BIKE":
                BikeBuilder bikeBuilder = new BikeBuilder();
                Bike bike = bikeRepository.save(bikeBuilder.getBike(vehicle));
                return bike;
            default:
                return null;
        }
    }
}
