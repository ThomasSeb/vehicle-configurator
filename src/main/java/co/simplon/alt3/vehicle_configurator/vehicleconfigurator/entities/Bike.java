package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class Bike extends Vehicle {

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('MOUNTAIN','ROAD','RACING')")
    private BikeEnumType bikeType;

    @Column(columnDefinition = "TINYINT unsigned")
    private Integer chainRings;
    private Boolean isMarked;

    public Bike() {
    }

    public BikeEnumType getBikeType() {
        return bikeType;
    }

    public void setBikeType(BikeEnumType bikeType) {
        this.bikeType = bikeType;
    }

    public Integer getChainRings() {
        return chainRings;
    }

    public void setChainRings(Integer chainRings) {
        this.chainRings = chainRings;
    }

    public Boolean getIsMarked() {
        return isMarked;
    }

    public void setIsMarked(Boolean isMarked) {
        this.isMarked = isMarked;
    }
    
}
