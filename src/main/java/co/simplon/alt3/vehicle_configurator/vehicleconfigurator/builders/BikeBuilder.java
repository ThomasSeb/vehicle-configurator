package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.builders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Bike;

public class BikeBuilder {

    private Bike bike;

    public Bike getBike(String json ){

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            bike = mapper.readValue(json, Bike.class);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return bike;
    }
}
