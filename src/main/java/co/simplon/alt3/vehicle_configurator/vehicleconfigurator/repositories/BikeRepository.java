package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Bike;

@Repository
public interface BikeRepository extends JpaRepository<Bike, Integer> {
    
}
