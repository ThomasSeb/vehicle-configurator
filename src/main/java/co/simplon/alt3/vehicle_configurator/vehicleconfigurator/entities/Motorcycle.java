package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class Motorcycle extends Vehicle {
    
    @Column(columnDefinition = "INT unsigned")
    private Integer kilometers;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate firstRegistration;

    @Column(columnDefinition = "SMALLINT unsigned")
    private Integer horsePower;

    @Column(columnDefinition = "SMALLINT unsigned")
    private Integer cubicCapacity;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('ROAD','ROADSTER','SPORTS','SUPERMOTO','CUSTOM')")
    private MotorcycleEnumType motoType;

    public Motorcycle() {
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    public LocalDate getFirstRegistration() {
        return firstRegistration;
    }

    public void setFirstRegistration(LocalDate firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public Integer getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Integer horsePower) {
        this.horsePower = horsePower;
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public MotorcycleEnumType getMotoType() {
        return motoType;
    }

    public void setMotoType(MotorcycleEnumType motoType) {
        this.motoType = motoType;
    }
}

