package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
    
}
