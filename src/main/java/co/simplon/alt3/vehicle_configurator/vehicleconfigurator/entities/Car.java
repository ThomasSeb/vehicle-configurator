package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@OnDelete(action = OnDeleteAction.CASCADE)
public class Car extends Vehicle {

    @Column(columnDefinition = "INT unsigned")
    private Integer kilometers;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate firstRegistration;
    
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('SEDAN','COUPE','WAGON','CONVERTIBLE','PICKUP','TOUTTERRAIN','SUV','SPORTS')")
    private CarEnumType carType;

    @Column(columnDefinition = "TINYINT unsigned")
    private Integer doorNbr;
    private Boolean isAutomatic;
    
    @Column(columnDefinition = "SMALLINT unsigned")
    private Integer horsePower;

    public Car() {
    }

    public Integer getKilometers() {
        return kilometers;
    }

    public void setKilometers(Integer kilometers) {
        this.kilometers = kilometers;
    }

    
    public LocalDate getFirstRegistration() {
        return firstRegistration;
    }

    public void setFirstRegistration(LocalDate firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public String getCarType() {       /*On change le type pour utiliser le getValue de notre CarEnumType*/
        return carType.getValue();
    }

    public void setCarType(CarEnumType carType) {
        this.carType = carType;
    }

    public Integer getDoorNbr() {
        return doorNbr;
    }

    public void setDoorNbr(Integer doorNbr) {
        this.doorNbr = doorNbr;
    }

    public Boolean getIsAutomatic() {
        return isAutomatic;
    }

    public void setIsAutomatic(Boolean isAutomatic) {
        this.isAutomatic = isAutomatic;
    }

    public Integer getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(Integer horsePower) {
        this.horsePower = horsePower;
    }

}