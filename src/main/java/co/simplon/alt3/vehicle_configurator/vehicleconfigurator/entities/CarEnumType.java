package co.simplon.alt3.vehicle_configurator.vehicleconfigurator.entities;

public enum CarEnumType {
    SEDAN("Sedan"),
    COUPE("Coupe"),
    WAGON("Wagon"),
    CONVERTIBLE("Convertible"),
    PICKUP("Pick-up"),
    TOUTTERRAIN("4x4"),
    SUV("SUV"),
    SPORTS("Sports");

    private String value;

    private CarEnumType(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
