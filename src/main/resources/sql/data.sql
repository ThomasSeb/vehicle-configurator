use vehicle_configurator;

insert into vehicle (brand, color, purchase_date, purchase_price, selling_price, vehicle_type) values ('Renault', 'Blue', '2022-05-24', 3521.32, 4999.99, 'CAR');
insert into car (car_type, door_nbr, first_registration, horse_power, is_automatic, kilometers, id) values ('WAGON', 5, '2012-06-14', 90, 0, 123654, 1);
insert into vehicle (brand, color, purchase_date, purchase_price, selling_price, vehicle_type) values ('Kawasaki', 'Red', '2022-05-20', 750.50, 999.99, 'MOTORCYCLE');
insert into motorcycle (cubic_capacity, first_registration, horse_power, kilometers, moto_type, id) values (125, '2013-08-15', 14, 23512, 'CUSTOM', 2);
insert into vehicle (brand, color, purchase_date, purchase_price, selling_price, vehicle_type) values ('Peugeot','Pink', '2022-05-15', 356.28, 599.99, 'BIKE');
insert into bike (bike_type, chain_rings, is_marked, id) values ('ROAD', 3, 1, 3);